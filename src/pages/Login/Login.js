import React, { useState, useContext } from 'react';
import './Login.scss';
import message from 'antd/lib/message';
import Axios from 'axios';
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';
import { CounselAppContext } from '../../Context_Api/Context';
import CounselLogo from '../../static/images/CounselLogo.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

function Login({ history }) {
  const [emailId, setemailId] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const { login, email } = useContext(CounselAppContext);
  const [mfaEnabled, setMfaEnabled] = useState(false);
  const [googlePin, setGooglePin] = useState('');
  const userLogin = (e) => {
    e.preventDefault();
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailId)) {
      setLoading(true);
      if (mfaEnabled) {
        Axios.post('https://gxauth.apimachine.com/gx/user/login', {
          email: emailId,
          password: password,
          totp_code: googlePin,
        })
          .then((response) => {
            const { data } = response;
            setLoading(false);
            if (data.status) {
              login(emailId, data.accessToken, data.idToken);
              if (history) {
                history.push('/');
              }
            } else {
              message.error(data.message);
            }
          })
          .finally(() => {
            setLoading(false);
          });
      } else {
        Axios.post('https://gxauth.apimachine.com/gx/user/login', {
          email: emailId,
          password: password,
        })
          .then((response) => {
            const { data } = response;
            setLoading(false);
            if (data.status) {
              login(emailId, data.accessToken, data.idToken);
              if (history) {
                history.push('/');
              }
            } else if (data.mfa) {
              setMfaEnabled(true);
            } else {
              message.error(data.message);
            }
          })
          .catch((error) => {
            message.error('Some Thing Went Wrong!');
            setLoading(false);
          });
      }
    } else {
      message.error('Enter Valid Email');
    }
  };
  return (
    <div className="login-ag">
      <div className="form-wrapper">
        <img src={CounselLogo} style={{ width: '10rem' }} alt="no_img" />
        <form className="login-form mx-auto" onSubmit={userLogin}>
          <Fade bottom>
            <div className="group">
              <input
                type="text"
                name="email"
                value={emailId}
                onChange={(e) => setemailId(e.target.value)}
                required="required"
              />
              <span className="highlight" />
              <span className="bar" />
              <label>Email</label>
            </div>
          </Fade>
          <Fade bottom>
            <div className="group">
              <input
                type="password"
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required="required"
              />
              <span className="highlight" />
              <span className="bar" />
              <label>Password</label>
            </div>
          </Fade>
          <Fade bottom>
            <button
              type="submit"
              disabled={loading}
              className="btn btn-darkblue mb-5"
            >
              {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'UNLOCK'}
            </button>
          </Fade>
        </form>
        {/* <button
          type="submit"
          className="btn login-in-login"
          onClick={userLogin}
        >
          Login
        </button> */}
      </div>
    </div>
  );
}

export default Login;
